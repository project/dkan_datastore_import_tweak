# DKAN Datastore Import Tweak
Finetune some features of DKAN datastore imports.
Currently just allowing to configure the CSV parser in terms of delimiter and quotes. 
## Features
Allows to configure delimiter and quoting character for DKAN datastore import, which is not possible directly via DKAN at this moment.

There is also a submodule datastore_mysql_import_tweak which allows to configure
the delimiter for the datastore_mysql_import module too.

## Post-Installation
configurable via: admin/dkan/parser-settings

## Additional Requirements
requires DKAN, specifically the datastore module

the datastore module needs to be enabled beforehand, it will not get
automatically enabled due to https://github.com/GetDKAN/dkan/discussions/4149

if using the MySql import (datastore_mysql_import), the datastore_mysql_import_tweak
module should be enabled (it needs datastore_mysql_import)
