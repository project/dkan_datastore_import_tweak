<?php

namespace Drupal\datastore_mysql_import_tweak\Factory;

/**
 * Mysql importer factory.
 */
class MysqlImportFactory extends \Drupal\datastore_mysql_import\Factory\MysqlImportFactory {

  /**
   * Inherited.
   *
   * @inheritdoc
   */
  public function getInstance(string $identifier, array $config = []) {
    $importer = parent::getInstance($identifier, $config);
    $importer->setImporterClass(\Drupal\datastore_mysql_import_tweak\Service\MysqlImport::class);
    return $importer;
  }

}
