<?php

namespace Drupal\datastore_mysql_import_tweak\Service;

use Drupal\Core\Database\Database;
use Procrastinator\Result;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

/**
 * MySQL LOAD DATA importer.
 *
 * @todo Figure out how to inject the file_system service into this class.
 */
class MysqlImport extends \Drupal\datastore_mysql_import\Service\MysqlImport {

  /**
   * Perform the import job.
   *
   * @return mixed
   *   The data to be placed in the Result object. This class does not use the
   *   result data, so it returns void.
   *
   * @throws \Exception
   *   Any exception thrown will be turned into an error in the Result object
   *   in the run() method.
   */
  protected function runIt() {
    // If the storage table already exists, we already performed an import and
    // can stop here.
    if ($this->dataStorage->hasBeenImported()) {
      $this->setStatus(Result::DONE);
      return NULL;
    }

    // Attempt to resolve resource file name from file path.
    if (($file_path = \Drupal::service('file_system')->realpath($this->resource->getFilePath())) === FALSE) {
      return $this->setResultError(sprintf('Unable to resolve file name "%s" for resource with identifier "%s".', $this->resource->getFilePath(), $this->resource->getId()));
    }

    // see https://github.com/GetDKAN/dkan/issues/4273
    $size = @filesize($file_path);
    if (!$size) {
      return $this->setResultError("Can't get size from file {$file_path}");
    }

    // Read the columns and EOL character sequence from the CSV file.
    $delimiter = $this->resource->getMimeType() == 'text/tab-separated-values' ? "\t" : \Drupal::config('dkan_datastore_import_tweak.parser_settings')->get('delimiter') ?? ',';

    try {
      [$columns, $column_lines] = $this->getColsFromFile($file_path, $delimiter);
    }
    catch (FileException $e) {
      return $this->setResultError($e->getMessage());
    }
    // Attempt to detect the EOL character sequence for this file; default to
    // '\n' on failure.
    $eol = $this->getEol($column_lines) ?? '\n';
    // Count the number of EOL characters in the header row to determine how
    // many lines the headers are occupying.
    $header_line_count = substr_count(trim($column_lines), self::EOL_TABLE[$eol]) + 1;
    // Generate sanitized table headers from column names.
    // Use headers to set the storage schema.
    $spec = $this->generateTableSpec($columns);
    $this->dataStorage->setSchema(['fields' => $spec]);

    // Count() will attempt to create the database table by side-effect of
    // calling setTable().
    $this->dataStorage->count();

    // Construct and execute a SQL import statement using the information
    // gathered from the CSV file being imported.
    $this->getDatabaseConnectionCapableOfDataLoad()->query(
      $this->getSqlStatement($file_path, $this->dataStorage->getTableName(), array_keys($spec), $eol, $header_line_count, $delimiter), [], ['allow_delimiter_in_query' => TRUE]);

    Database::setActiveConnection();

    $this->setStatus(Result::DONE);
    return NULL;
  }

}
