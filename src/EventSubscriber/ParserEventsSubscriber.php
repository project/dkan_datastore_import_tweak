<?php

namespace Drupal\dkan_datastore_import_tweak\EventSubscriber;

use Drupal\common\Events\Event;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\datastore\Service\ImportService;
use Drupal\dkan_datastore_import_tweak\Form\ParserSettingsForm;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ParserEventsSubscriber implements EventSubscriberInterface {

  /**
   * @var ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $parserConfig;

  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    $this->parserConfig = $config_factory->get(ParserSettingsForm::CONFIG_NAME);
  }


  public static function getSubscribedEvents() {
    return [
      ImportService::EVENT_CONFIGURE_PARSER => 'configureParser',
    ];
  }

  /**
   * change csv parser configuration
   *
   * @param Event $event
   * @return void
   * @throws \Exception
   */
  public function configureParser(Event $event) {
    $parser_config = $event->getData();
    $parser_config['delimiter'] = $this->parserConfig->get('delimiter');
    $parser_config['quote'] = $this->parserConfig->get('quote');
    $event->setData($parser_config);
  }
}
