<?php

declare(strict_types=1);

namespace Drupal\dkan_datastore_import_tweak\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure DKAN Datastore Import Tweak settings for this site.
 */
final class ParserSettingsForm extends ConfigFormBase {

  public const CONFIG_NAME = 'dkan_datastore_import_tweak.parser_settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'dkan_datastore_import_tweak_parser_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [$this::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['delimiter'] = [
      '#type' => 'select',
      '#title' => $this->t('Delimiter'),
      '#options' => [
        ',' => ',',
        ';' => ';',
        ' ' => $this->t('Whitespace')
      ],
      '#default_value' => $this->config($this::CONFIG_NAME)->get('delimiter') ?? ',',
    ];
    $form['quote'] = [
      '#type' => 'select',
      '#title' => $this->t('Quoting character'),
      '#options' => [
        '"' => '"',
        '\'' => '\'',
      ],
      '#default_value' => $this->config($this::CONFIG_NAME)->get('quote') ?? '"',
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    // @todo Validate the form here.
    // Example:
    // @code
    //   if ($form_state->getValue('example') === 'wrong') {
    //     $form_state->setErrorByName(
    //       'message',
    //       $this->t('The value is not correct.'),
    //     );
    //   }
    // @endcode
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config($this::CONFIG_NAME)
      ->set('delimiter', $form_state->getValue('delimiter'))
      ->save();
    $this->config($this::CONFIG_NAME)
      ->set('quote', $form_state->getValue('quote'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
